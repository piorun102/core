package cacheUC

import (
	"fmt"
	"gitlab.com/piorun102/core/cache"
	"gitlab.com/piorun102/core/protobuf"
	"gitlab.com/piorun102/lg"
	bwgNats "gitlab.com/piorun102/pkg/storage/nats"
)

type Client struct {
	nc bwgNats.NC
}

func New(nc bwgNats.NC) cache.UC {
	return &Client{
		nc: nc,
	}
}

const UsersBalanceCache = "usersBalance"

func (c *Client) InsertUsersBalancesCache(ctx lg.CtxLogger, input *protobuf.BalanceFullMap) error {
	return c.nc.InsertCache(ctx, UsersBalanceCache, input)
}
func (c *Client) SelectUsersBalancesCache(ctx lg.CtxLogger, req *protobuf.SelectBalancesRequest) (res *protobuf.BalanceFullMap, err error) {
	var output = &protobuf.BalanceFullMap{}
	res = &protobuf.BalanceFullMap{Balance: make(map[string]*protobuf.BalanceFull)}
	err = c.nc.SelectCache(ctx, UsersBalanceCache, output)
	if err != nil {
		return
	}
	for _, address := range req.Addresses {
		if balance, ok := output.Balance[address.ToString()]; !ok {
			res.Balance[address.ToString()] = &protobuf.BalanceFull{
				Available: 0,
				Freeze:    0,
			}
		} else {
			res.Balance[address.ToString()] = balance
		}
	}
	return
}
func (c *Client) SelectAllUsersBalancesCache(ctx lg.CtxLogger) (res *protobuf.BalanceFullMap, err error) {
	var output = &protobuf.BalanceFullMap{}
	res = &protobuf.BalanceFullMap{Balance: make(map[string]*protobuf.BalanceFull)}
	err = c.nc.SelectCache(ctx, UsersBalanceCache, output)
	res = output
	if res.Balance == nil {
		res.Balance = make(map[string]*protobuf.BalanceFull)
	}
	return
}

const OfferInfoCache = "offerInfo"

func (c *Client) InsertOfferInfoCache(ctx lg.CtxLogger, input *protobuf.OfferInfoMap) error {
	return c.nc.InsertCache(ctx, OfferInfoCache, input)
}
func (c *Client) SelectOfferInfoCache(ctx lg.CtxLogger, req *protobuf.Address) (res *protobuf.OfferInfo, err error) {
	var (
		output   = &protobuf.OfferInfoMap{}
		tempAddr = &protobuf.Address{
			ID:          OfferWithOffsetN(req.ID),
			TokenTypeID: req.TokenTypeID,
		}
		ok bool
	)
	res = &protobuf.OfferInfo{}
	err = c.nc.SelectCache(ctx, OfferInfoCache, output)
	if err != nil {
		return
	}
	res, ok = output.OfferInfo[tempAddr.ToString()]
	if !ok {
		err = fmt.Errorf("offer info not found for address %v", req.String())
		return
	}
	return
}

const OrderMarginCache = "OrderMargin"

func (c *Client) InsertOrderMarginCache(ctx lg.CtxLogger, input *protobuf.OrderMarginMap) error {
	return c.nc.InsertCache(ctx, OrderMarginCache, input)
}
func (c *Client) SelectOrderMarginCache(ctx lg.CtxLogger) (res map[int64]float64, err error) {
	var output = &protobuf.OrderMarginMap{}
	res = make(map[int64]float64)
	err = c.nc.SelectCache(ctx, OrderMarginCache, output)
	if err != nil {
		return
	}
	res = output.OrderMargin
	return
}

const UserAutoDepositCache = "UserAutoDeposit"

func (c *Client) InsertUserAutoDeposit(ctx lg.CtxLogger, req map[int64]bool) error {
	return c.nc.InsertCacheJSON(ctx, UserAutoDepositCache, req)
}
func (c *Client) SelectUserAutoDeposit(ctx lg.CtxLogger) (res map[int64]bool, err error) {
	res = make(map[int64]bool)
	err = c.nc.SelectCacheJSON(ctx, UserAutoDepositCache, &res)
	if err != nil {
		return
	}
	return
}

const (
	offerOffset = 1_000_000_000_000
)

func OfferWithOffsetN(req int64) int64 {
	if req > offerOffset {
		return req
	}
	req = req + offerOffset
	return req
}

const UsersDepositsCache = "UsersDeposits"

func (c *Client) InsertUsersDeposits(ctx lg.CtxLogger, input *protobuf.UsersDeposits) error {
	return c.nc.InsertCache(ctx, UsersDepositsCache, input)
}
func (c *Client) SelectUsersDeposits(ctx lg.CtxLogger) (res []*protobuf.UserDeposit, err error) {
	var output = &protobuf.UsersDeposits{}
	err = c.nc.SelectCache(ctx, UsersDepositsCache, output)
	if err != nil {
		return
	}
	if output.GetData() == nil {
		err = fmt.Errorf("nil data")
		return
	}
	res = output.GetData()
	return
}
