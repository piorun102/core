package cache

import (
	"gitlab.com/piorun102/core/protobuf"
	"gitlab.com/piorun102/lg"
)

type UC interface {
	SelectUsersBalancesCache(ctx lg.CtxLogger, req *protobuf.SelectBalancesRequest) (res *protobuf.BalanceFullMap, err error)
	SelectOfferInfoCache(ctx lg.CtxLogger, req *protobuf.Address) (res *protobuf.OfferInfo, err error)
	SelectAllUsersBalancesCache(ctx lg.CtxLogger) (res *protobuf.BalanceFullMap, err error)
	SelectOrderMarginCache(ctx lg.CtxLogger) (res map[int64]float64, err error)
	SelectUserAutoDeposit(ctx lg.CtxLogger) (res map[int64]bool, err error)
	SelectUsersDeposits(ctx lg.CtxLogger) (res []*protobuf.UserDeposit, err error)

	InsertUsersDeposits(ctx lg.CtxLogger, input *protobuf.UsersDeposits) error
	InsertUsersBalancesCache(ctx lg.CtxLogger, input *protobuf.BalanceFullMap) error
	InsertOfferInfoCache(ctx lg.CtxLogger, input *protobuf.OfferInfoMap) error
	InsertOrderMarginCache(ctx lg.CtxLogger, input *protobuf.OrderMarginMap) error
	InsertUserAutoDeposit(ctx lg.CtxLogger, req map[int64]bool) error
}
