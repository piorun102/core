package processing

import (
	"gitlab.com/piorun102/core/protobuf"
	"gitlab.com/piorun102/lg"
)

type UC interface {
	SwapInfo(ctx lg.CtxLogger, req *protobuf.SwapInfoReq) (resp *protobuf.SwapInfoData, err error)
	SwapCreate(ctx lg.CtxLogger, req *protobuf.SwapCreateReq) (resp *protobuf.SwapCreateData, err error)
	SwapInit(ctx lg.CtxLogger, req *protobuf.SwapInitReq) (resp *protobuf.SwapInitData, revert bool, err error)
	// WithdrawCreate предварительно создаем запись в бд, что хотим вывести, получаем ID для создания трензакции
	WithdrawCreate(ctx lg.CtxLogger, req *protobuf.WithdrawCreateRequest) (ID int64, commission float64, err error)
	// WithdrawInit вписываем хеш транзакционной систеиы, затем запрос в кп, затем отвечаем успехом
	WithdrawInit(ctx lg.CtxLogger, req *protobuf.WithdrawInitRequest) (procID int64, err error)
	SelectOrInsertWallet(ctx lg.CtxLogger, req *protobuf.SelectOrInsertWalletRequest) (address, internalID string, err error)
	SelectProcTransactionsByUser(ctx lg.CtxLogger, req *protobuf.SelectProcTransactionsByUserRequest) (txs []*protobuf.ProcTransaction, err error)
	SelectProcBalance(ctx lg.CtxLogger, req *protobuf.SelectProcBalanceRequest) (balance float64, err error)
	SelectTurnover(ctx lg.CtxLogger, req *protobuf.SelectTurnoverRequest) (turnover *protobuf.Turnover, err error)
	SelectUserCommission(ctx lg.CtxLogger, req *protobuf.SelectUserCommissionRequest) (userCommission *protobuf.UserCommission, err error)
	BatchByCrypto(ctx lg.CtxLogger, req *protobuf.BatchByCryptoRequest) (txs []*protobuf.Crypto, err error)

	CreateTransferBalance(ctx lg.CtxLogger, req *protobuf.TransferBalanceCreateRequest) (res string, err error)
	UpdateTransferBalance(ctx lg.CtxLogger, req *protobuf.TransferBalanceUpdateRequest) (res string, err error)
	TransferBalances(ctx lg.CtxLogger, req *protobuf.TransferBalancesRequest) (res *protobuf.TransferLogData, err error)
	//SelectTransactionHistory(ctx lg.CtxLogger, req *protobuf.SelectTransactionHistoryRequest) (txs []*protobuf.ProcTransaction, err error)
	//SelectTransaction(ctx lg.CtxLogger, req *protobuf.SelectTransactionRequest) (tx *protobuf.ProcTransaction, err error)
	//FetchTransaction(ctx lg.CtxLogger, req *protobuf.TransactionFetchRequest) (txs []*protobuf.ProcTransaction, err error)
	//SelectUserWallets(ctx lg.CtxLogger, req *protobuf.SelectUserWalletRequest) (wallets []*protobuf.Wallet, err error)
}
