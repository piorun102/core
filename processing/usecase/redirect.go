package processingUC

const (
	SelectSwapByUser              = "txProcessing.SelectSwapByUser"
	BatchBySwap                   = "txProcessing.BatchBySwap"
	GetProcTransactionByTrackerID = "txProcessing.GetProcTransactionByTrackerID"
	SelectProcTransactionsByUsers = "txProcessing.SelectProcTransactionsByUsers"
	SelectCommissions             = "txProcessing.SelectCommissions"
)
