package processingUC

import (
	"fmt"
	"gitlab.com/piorun102/core/processing"
	"gitlab.com/piorun102/core/protobuf"
	"gitlab.com/piorun102/lg"
	bwgNats "gitlab.com/piorun102/pkg/storage/nats"
)

type uC struct {
	nc bwgNats.NC
}

func New(nc bwgNats.NC) processing.UC {
	return &uC{
		nc: nc,
	}
}

const WithdrawCreate = "txProcessing.WithdrawCreate"

func (u *uC) WithdrawCreate(ctx lg.CtxLogger, req *protobuf.WithdrawCreateRequest) (ID int64, commission float64, err error) {
	resp := &protobuf.WithdrawCreateResponse{}
	if err = u.nc.Request(ctx, WithdrawCreate, req, resp); err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	if resp.Data == nil {
		err = fmt.Errorf("empty data")
		return
	}

	ID = resp.Data.ID
	commission = resp.Data.Commission

	ctx.Debugf("Transaction withdraw created with ID: %v", ID)

	return ID, commission, nil
}

const WithdrawInit = "txProcessing.WithdrawInit"

func (u *uC) WithdrawInit(ctx lg.CtxLogger, req *protobuf.WithdrawInitRequest) (procID int64, err error) {
	resp := &protobuf.WithdrawInitResponse{}
	if err = u.nc.Request(ctx, WithdrawInit, req, resp); err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	procID = resp.ProcID
	return
}

const SelectOrInsertWallet = "txProcessing.SelectOrInsertWallet"

func (u *uC) SelectOrInsertWallet(ctx lg.CtxLogger, req *protobuf.SelectOrInsertWalletRequest) (address, internalID string, err error) {
	var resp = &protobuf.SelectOrInsertWalletResponse{}
	err = u.nc.Request(ctx, SelectOrInsertWallet, req, resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	if resp.Address == "" {
		err = fmt.Errorf("empty address")
		return
	}
	address = resp.Address
	internalID = resp.InternalID
	return
}

const SelectProcTransactionsByUser = "txProcessing.SelectProcTransactionsByUser"

func (u *uC) SelectProcTransactionsByUser(ctx lg.CtxLogger, req *protobuf.SelectProcTransactionsByUserRequest) (txs []*protobuf.ProcTransaction, err error) {
	var resp = &protobuf.SelectProcTransactionsByUserResponse{}
	err = u.nc.Request(ctx, SelectProcTransactionsByUser, req, resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	txs = resp.Data
	return
}

const SelectProcBalance = "txProcessing.SelectProcBalance"

func (u *uC) SelectProcBalance(ctx lg.CtxLogger, req *protobuf.SelectProcBalanceRequest) (balance float64, err error) {
	var resp = &protobuf.SelectProcBalanceResponse{}
	err = u.nc.Request(ctx, SelectProcBalance, req, resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	balance = resp.Balance
	return
}

const SelectTurnover = "txProcessing.SelectTurnover"

func (u *uC) SelectTurnover(ctx lg.CtxLogger, req *protobuf.SelectTurnoverRequest) (turnover *protobuf.Turnover, err error) {
	var resp = &protobuf.SelectTurnoverResponse{}
	err = u.nc.Request(ctx, SelectTurnover, req, resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	if resp.Data == nil {
		err = fmt.Errorf("empty address")
		return
	}
	turnover = resp.Data
	return
}

const SelectUserCommission = "txProcessing.SelectUserCommission"

func (u *uC) SelectUserCommission(ctx lg.CtxLogger, req *protobuf.SelectUserCommissionRequest) (userCommission *protobuf.UserCommission, err error) {
	var resp = &protobuf.SelectUserCommissionResponse{}
	err = u.nc.Request(ctx, SelectUserCommission, req, resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	if resp.Data == nil {
		err = fmt.Errorf("empty address")
		return
	}
	userCommission = resp.Data
	return
}

const BatchByCrypto = "txProcessing.BatchByCrypto"

func (u *uC) BatchByCrypto(ctx lg.CtxLogger, req *protobuf.BatchByCryptoRequest) (txs []*protobuf.Crypto, err error) {
	resp := &protobuf.BatchByCryptoResponse{}
	if err = u.nc.Request(ctx, BatchByCrypto, req, resp); err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	if resp.Data == nil {
		err = fmt.Errorf("empty data")
		return
	}

	txs = resp.Data

	return txs, nil
}

const CreateTransferBalance = "txProcessing.CreateTransferBalance"

func (u *uC) CreateTransferBalance(ctx lg.CtxLogger, req *protobuf.TransferBalanceCreateRequest) (res string, err error) {
	resp := &protobuf.TransferBalanceCreateResponse{}
	if err = u.nc.Request(ctx, CreateTransferBalance, req, resp); err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	res = resp.Reason
	return
}

const TransferBalanceUpdate = "txProcessing.TransferBalanceUpdate"

func (u *uC) UpdateTransferBalance(ctx lg.CtxLogger, req *protobuf.TransferBalanceUpdateRequest) (res string, err error) {
	resp := &protobuf.TransferBalanceUpdateResponse{}
	if err = u.nc.Request(ctx, TransferBalanceUpdate, req, resp); err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	res = resp.Reason
	return
}

const SelectTransferBalance = "txProcessing.SelectTransferBalances"

func (u *uC) TransferBalances(ctx lg.CtxLogger, req *protobuf.TransferBalancesRequest) (res *protobuf.TransferLogData, err error) {
	resp := &protobuf.TransferBalancesResponse{}
	if err = u.nc.Request(ctx, SelectTransferBalance, req, resp); err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	res = resp.Data
	return
}

const SwapCreate = "txProcessing.SwapCreate"

func (u *uC) SwapCreate(ctx lg.CtxLogger, req *protobuf.SwapCreateReq) (resp *protobuf.SwapCreateData, err error) {
	output := &protobuf.SwapCreateResp{}
	if err = u.nc.Request(ctx, SwapCreate, req, output); err != nil {
		return
	}
	if !output.Success {
		err = fmt.Errorf(output.Reason)
		return
	}
	if output.GetData() == nil {
		err = fmt.Errorf("nil data")
		return
	}
	resp = output.GetData()
	return
}

const SwapInit = "txProcessing.SwapInit"

func (u *uC) SwapInit(ctx lg.CtxLogger, req *protobuf.SwapInitReq) (resp *protobuf.SwapInitData, revert bool, err error) {
	output := &protobuf.SwapInitResp{}
	if err = u.nc.Request(ctx, SwapInit, req, output); err != nil {
		return
	}
	if !output.Success {
		err = fmt.Errorf(output.Reason)
		return
	}
	revert = output.GetRevert()
	resp = output.GetData()
	return
}

const SwapInfo = "txProcessing.SwapInfo"

func (u *uC) SwapInfo(ctx lg.CtxLogger, req *protobuf.SwapInfoReq) (resp *protobuf.SwapInfoData, err error) {
	output := &protobuf.SwapInfoResp{}
	if err = u.nc.Request(ctx, SwapInfo, req, output); err != nil {
		return
	}
	if !output.Success {
		err = fmt.Errorf(output.Reason)
		return
	}
	if output.GetData() == nil {
		err = fmt.Errorf("nil data")
		return
	}
	resp = output.GetData()
	return
}
