package protobuf

import (
	"fmt"
	"github.com/hashicorp/go-secure-stdlib/parseutil"
	"gitlab.com/piorun102/lg"
	"strings"
)

func (a *Address) ToString() (res string) {
	return fmt.Sprintf("%d %d", a.ID, a.TokenTypeID)
}
func FromString(s string) (res *Address) {
	var (
		slice []int
		err   error
	)
	slice, err = parseutil.SafeParseIntSlice(strings.Split(s, " "), 2)
	if err != nil {
		lg.Fatalf("%v", err)
		return
	}
	res = &Address{
		ID:          int64(slice[0]),
		TokenTypeID: TokenTypeID(slice[1]),
	}
	return
}
