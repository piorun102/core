package protobuf

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_FromString(t *testing.T) {
	addrStart := Address{
		ID:          1,
		TokenTypeID: 1110,
	}
	addr := FromString(addrStart.ToString())
	if assert.NotEqual(t, nil, addr) {
		assert.Equal(t, addrStart.String(), addr.String())
	}
}
