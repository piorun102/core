package protobuf

// Direction должна ли транзакция изменять баланс
func (tx TxTypeID) Direction() int {
	if tx == TxTypeID_Mint || tx == TxTypeID_Squash {
		return 1
	} else if tx == TxTypeID_Burn {
		return -1
	}
	return 0
}
