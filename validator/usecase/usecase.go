package validatorUC

import (
	"fmt"
	"gitlab.com/piorun102/core/protobuf"
	"gitlab.com/piorun102/core/validator"
	"gitlab.com/piorun102/lg"
	bwgNats "gitlab.com/piorun102/pkg/storage/nats"
	"strconv"
)

type Client struct {
	nc bwgNats.NC
}

func New(nc bwgNats.NC) validator.UC {
	return &Client{
		nc: nc,
	}
}

const SendTransaction = "txValidator.SendTransaction"

func (c *Client) SendTransaction(ctx lg.CtxLogger, req *protobuf.SendTransactionRequest) (ID *int64, err error) {
	res := &protobuf.SendTransactionResponse{}
	err = c.nc.Request(ctx, SendTransaction, req, res)
	if err != nil {
		return
	}
	if !res.Success {
		err = fmt.Errorf(res.Reason)
		return
	}
	if res.ID == nil {
		err = fmt.Errorf("nil id")
		return
	}
	ID = res.ID
	return
}

const UpdateTransaction = "txValidator.UpdateTransaction"

func (c *Client) UpdateTransaction(ctx lg.CtxLogger, req *protobuf.UpdateTransactionRequest) (err error) {
	resp := &protobuf.UpdateTransactionResponse{}
	err = c.nc.Request(ctx, UpdateTransaction, req, resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	return
}

const SelectBalances = "txValidator.SelectBalances"

func (c *Client) SelectBalances(ctx lg.CtxLogger, req *protobuf.SelectBalancesRequest) (balances map[string]*protobuf.Balance, err error) {
	res := &protobuf.SelectBalancesResponse{}
	err = c.nc.Request(ctx, SelectBalances, req, res)
	if err != nil {
		return
	}
	if !res.Success {
		err = fmt.Errorf(res.Reason)
		return
	}
	if len(res.OwnedBalances) == 0 {
		err = fmt.Errorf("balances is empty")
		return
	}
	balances = res.OwnedBalances
	return
}

const SelectAllBalance = "txValidator.SelectAllBalance"

func (c *Client) SelectAllBalance(ctx lg.CtxLogger, req *protobuf.SelectAllBalanceReq) (balance *float64, err error) {
	res := &protobuf.SelectAllBalanceResponse{}
	err = c.nc.Request(ctx, SelectAllBalance, req, res)
	if err != nil {
		return
	}
	if !res.Success {
		err = fmt.Errorf(res.Reason)
		return
	}
	floatNum, err := strconv.ParseFloat(res.Data.Balance, 64)
	if err != nil {
		return
	}

	balance = &floatNum
	return
}

const SelectOfferBalance = "txValidator.SelectOfferBalance"

func (c *Client) SelectOfferBalance(ctx lg.CtxLogger, req *protobuf.SelectOfferBalanceReq) (offerBalance *protobuf.OfferInfo, err error) {
	res := &protobuf.SelectOfferBalanceResponse{}
	err = c.nc.Request(ctx, SelectOfferBalance, req, res)
	if err != nil {
		return
	}
	if !res.Success {
		err = fmt.Errorf(res.Reason)
		return
	}
	if res.Data == nil {
		err = fmt.Errorf("empty SelectOfferBalance data")
	}
	offerBalance = res.Data
	return
}

func (c *Client) SelectBalance(ctx lg.CtxLogger, req *protobuf.Address) (balance *protobuf.Balance, err error) {
	var (
		response = map[string]*protobuf.Balance{}
		ok       bool
		ob       = &protobuf.Balance{}
	)

	response, err = c.SelectBalances(
		ctx,
		&protobuf.SelectBalancesRequest{
			Addresses: []*protobuf.Address{req},
		},
	)
	if err != nil {
		return
	}
	if ob, ok = response[req.ToString()]; !ok {
		err = fmt.Errorf("balance not found for address %v", req.String())
		return
	}
	balance = ob
	return
}

const SendTransactionWithTxID = "txValidator.SendTransactionWithTxID"

func (c *Client) SendTransactionWithTxID(ctx lg.CtxLogger, req *protobuf.SendTransactionWithTxIDRequest) (ID *int64, err error) {
	res := &protobuf.SendTransactionWithTxIDResponse{}
	err = c.nc.Request(ctx, SendTransactionWithTxID, req, res)
	if err != nil {
		return
	}
	if !res.Success {
		err = fmt.Errorf(res.Reason)
		return
	}
	if res.ID == nil {
		err = fmt.Errorf("nil id")
		return
	}
	ID = res.ID
	return
}

const SelectBalanceHistory = "txValidator.SelectBalanceHistory"
const SelectMargin = "txValidator.SelectMargin"
