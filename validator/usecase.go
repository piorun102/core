package validator

import (
	"gitlab.com/piorun102/core/protobuf"
	"gitlab.com/piorun102/lg"
)

type UC interface {
	SendTransaction(ctx lg.CtxLogger, req *protobuf.SendTransactionRequest) (ID *int64, err error)
	UpdateTransaction(ctx lg.CtxLogger, req *protobuf.UpdateTransactionRequest) (err error)
	SelectBalances(ctx lg.CtxLogger, req *protobuf.SelectBalancesRequest) (balances map[string]*protobuf.Balance, err error)
	SelectAllBalance(ctx lg.CtxLogger, req *protobuf.SelectAllBalanceReq) (balance *float64, err error)
	SelectBalance(ctx lg.CtxLogger, req *protobuf.Address) (balance *protobuf.Balance, err error)
	SelectOfferBalance(ctx lg.CtxLogger, req *protobuf.SelectOfferBalanceReq) (offerBalance *protobuf.OfferInfo, err error)
	SendTransactionWithTxID(ctx lg.CtxLogger, req *protobuf.SendTransactionWithTxIDRequest) (ID *int64, err error)
}
