PHONE: compile-proto

compile-proto:
	# Удаляем все указанные файлы в директории output
	rm -fr 'protobuf/*.pb.go'
	export PATH="$PATH:$GOPATH/bin"
	protoc -I. shared/protobuf/*.proto validator/protobuf/*.proto processing/protobuf/*.proto handler/protobuf/*.proto --go_out=protobuf  --experimental_allow_proto3_optional
	mv protobuf/google.golang.org/protobuf/* protobuf
	rm -rf protobuf/google.golang.org

compile-proto-win:
	Remove-Item -Path protobuf/*.pb.go -Force
	protoc -I. shared/protobuf/*.proto validator/protobuf/*.proto processing/protobuf/*.proto handler/protobuf/*.proto --go_out=protobuf --experimental_allow_proto3_optional
	move protobuf/google.golang.org/protobuf/* protobuf
	Remove-Item -Path protobuf/google.golang.org -Recurse -Force