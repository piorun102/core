package handler

import (
	"gitlab.com/piorun102/core/protobuf"
	"gitlab.com/piorun102/lg"
)

type UC interface {
	BuildTransaction(ctx lg.CtxLogger, req *protobuf.BuildRequest) (ID int64, err error)
	BuildTransactionV2(ctx lg.CtxLogger, req *protobuf.BuildRequestV2) (ID int64, err error)
	UpdateTransactionByID(ctx lg.CtxLogger, req *protobuf.UpdateTransactionByIDRequest) (err error)
	UpdateTransactionByPath(ctx lg.CtxLogger, req *protobuf.UpdateTransactionByPathRequest) (err error)
	SquashBalance(ctx lg.CtxLogger, req *protobuf.SquashRequest) (ID int64, err error)
	SelectBalances(ctx lg.CtxLogger, req *protobuf.SelectBalancesRequest) (ownedBalances map[string]*protobuf.Balance, err error)
	SelectBalance(ctx lg.CtxLogger, req *protobuf.Address) (balance *protobuf.Balance, err error)
	SelectAllBalance(ctx lg.CtxLogger, req *protobuf.SelectAllBalanceReq) (allBalance *protobuf.AllBalance, err error)
	SelectBalanceHistory(ctx lg.CtxLogger, req *protobuf.SelectBalanceHistoryReq) (balance []*protobuf.BalanceHistory, err error)
	SelectMargin(ctx lg.CtxLogger, req *protobuf.SelectMarginReq) (balance float64, err error)

	SelectOrInsertWallet(ctx lg.CtxLogger, req *protobuf.SelectOrInsertWalletRequest) (address, internalID string, err error)
	SelectProcTransactionsByUser(ctx lg.CtxLogger, req *protobuf.SelectProcTransactionsByUserRequest) (txs []*protobuf.ProcTransaction, err error)
	SelectProcBalance(ctx lg.CtxLogger, req *protobuf.SelectProcBalanceRequest) (balance float64, err error)
	SelectTurnover(ctx lg.CtxLogger, req *protobuf.SelectTurnoverRequest) (turnover *protobuf.Turnover, err error)
	SelectUserCommission(ctx lg.CtxLogger, req *protobuf.SelectUserCommissionRequest) (userCommission *protobuf.UserCommission, err error)
	BatchByCrypto(ctx lg.CtxLogger, req *protobuf.BatchByCryptoRequest) (txs []*protobuf.Crypto, err error)
	SwapInfo(ctx lg.CtxLogger, req *protobuf.SwapInfoReq) (resp *protobuf.SwapInfoData, err error)
	// redirect to processing
	CreateTransferBalance(ctx lg.CtxLogger, req *protobuf.TransferBalanceCreateRequest) (res string, err error)
	UpdateTransferBalance(ctx lg.CtxLogger, req *protobuf.TransferBalanceUpdateRequest) (res string, err error)
	TransferBalances(ctx lg.CtxLogger, req *protobuf.TransferBalancesRequest) (res *protobuf.TransferLogData, err error)

	SelectSwapByUser(ctx lg.CtxLogger, req *protobuf.SelectSwapByUserReq) (txs *protobuf.ProcSwapData, err error)
	BatchBySwap(ctx lg.CtxLogger, req *protobuf.BatchBySwapReq) (txs []*protobuf.ProcSwap, err error)

	SelectProcTransactionsByUsers(ctx lg.CtxLogger, req *protobuf.SelectProcTransactionsByUsersReq) (resp []*protobuf.ProcTransaction, err error)
	GetProcTransactionByTrackerID(ctx lg.CtxLogger, req *protobuf.GetProcTransactionByTrackerIDReq) (resp *protobuf.ProcTransaction, err error)
	SelectCommissions(ctx lg.CtxLogger, req *protobuf.SelectCommissionsReq) (resp []*protobuf.Commission, err error)
}
