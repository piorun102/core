package handlerUC

import (
	"fmt"
	"gitlab.com/piorun102/core/handler"
	"gitlab.com/piorun102/core/protobuf"
	"gitlab.com/piorun102/lg"
	bwgNats "gitlab.com/piorun102/pkg/storage/nats"
)

type uC struct {
	lg lg.Logger
	nc bwgNats.NC
}

func New(nc bwgNats.NC) handler.UC {
	return &uC{
		nc: nc,
	}
}

const BuildTransaction = "handler.BuildTransaction"

func (u *uC) BuildTransaction(ctx lg.CtxLogger, req *protobuf.BuildRequest) (ID int64, err error) {
	var resp protobuf.BuildResponse
	err = u.nc.Request(ctx, BuildTransaction, req, &resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	if resp.ID == nil {
		err = fmt.Errorf("empty ID")
		return
	}
	ID = *resp.ID
	ctx.Debugf("Transaction created with ID: %v", ID)
	return
}

const BuildTransactionV2 = "handler.BuildTransactionV2"

func (u *uC) BuildTransactionV2(ctx lg.CtxLogger, req *protobuf.BuildRequestV2) (ID int64, err error) {
	var resp protobuf.BuildResponse
	err = u.nc.Request(ctx, BuildTransactionV2, req, &resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	if resp.ID == nil {
		err = fmt.Errorf("empty ID")
		return
	}
	ID = *resp.ID
	ctx.Debugf("Transaction created with ID: %v", ID)
	return
}

const UpdateTransactionByID = "handler.UpdateTransactionByID"

func (u *uC) UpdateTransactionByID(ctx lg.CtxLogger, req *protobuf.UpdateTransactionByIDRequest) (err error) {
	var resp protobuf.UpdateTransactionResponse
	err = u.nc.Request(ctx, UpdateTransactionByID, req, &resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	return
}

const UpdateTransactionByPath = "handler.UpdateTransactionByPath"

func (u *uC) UpdateTransactionByPath(ctx lg.CtxLogger, req *protobuf.UpdateTransactionByPathRequest) (err error) {
	var resp protobuf.UpdateTransactionResponse
	err = u.nc.Request(ctx, UpdateTransactionByPath, req, &resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	return
}

const SquashBalance = "handler.SquashBalance"

func (u *uC) SquashBalance(ctx lg.CtxLogger, req *protobuf.SquashRequest) (ID int64, err error) {
	var resp protobuf.BuildResponse
	err = u.nc.Request(ctx, SquashBalance, req, &resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	if resp.ID == nil {
		err = fmt.Errorf("empty ID")
		return
	}
	ID = *resp.ID
	ctx.Debugf("Transaction created with ID: %v", ID)
	return
}

const SelectBalances = "handler.SelectBalances"

func (u *uC) SelectBalances(ctx lg.CtxLogger, req *protobuf.SelectBalancesRequest) (ownedBalances map[string]*protobuf.Balance, err error) {
	var resp protobuf.SelectBalancesResponse
	err = u.nc.Request(ctx, SelectBalances, req, &resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	if resp.OwnedBalances == nil {
		err = fmt.Errorf("empty OwnedBalances")
		return
	}
	ownedBalances = resp.OwnedBalances
	return
}
func (u *uC) SelectBalance(ctx lg.CtxLogger, req *protobuf.Address) (balance *protobuf.Balance, err error) {
	var (
		ownedBalances map[string]*protobuf.Balance
		ok            bool
	)
	ownedBalances, err = u.SelectBalances(ctx, &protobuf.SelectBalancesRequest{Addresses: []*protobuf.Address{req}})
	if err != nil {
		return
	}
	if balance, ok = ownedBalances[req.ToString()]; !ok {
		err = fmt.Errorf("balance not found")
		return
	}
	return
}

const SelectAllBalance = "handler.SelectAllBalance"

func (u *uC) SelectAllBalance(ctx lg.CtxLogger, req *protobuf.SelectAllBalanceReq) (allBalance *protobuf.AllBalance, err error) {
	var resp protobuf.SelectAllBalanceResponse
	err = u.nc.Request(ctx, SelectAllBalance, req, &resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	if resp.Data == nil {
		err = fmt.Errorf("empty OwnedBalances")
		return
	}
	allBalance = resp.Data
	return
}

const SelectOffersBalances = "handler.SelectOffersBalances"

func (u *uC) SelectOffersBalances(ctx lg.CtxLogger, req *protobuf.SelectBalancesRequest) (resp *protobuf.SelectBalancesResponse) {
	resp = &protobuf.SelectBalancesResponse{}
	err := u.nc.Request(ctx, SelectOffersBalances, req, resp)
	if err != nil {
		resp.Reason = err.Error()
	}
	return resp
}

const SelectOrInsertWallet = "handler.SelectOrInsertWallet"

func (u *uC) SelectOrInsertWallet(ctx lg.CtxLogger, req *protobuf.SelectOrInsertWalletRequest) (address, internalID string, err error) {
	var resp = &protobuf.SelectOrInsertWalletResponse{}
	err = u.nc.Request(ctx, SelectOrInsertWallet, req, resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	if resp.Address == "" {
		err = fmt.Errorf("empty address")
		return
	}
	address = resp.Address
	internalID = resp.InternalID
	return
}

const SelectProcTransactionsByUser = "handler.SelectProcTransactionsByUser"

func (u *uC) SelectProcTransactionsByUser(ctx lg.CtxLogger, req *protobuf.SelectProcTransactionsByUserRequest) (txs []*protobuf.ProcTransaction, err error) {
	var resp = &protobuf.SelectProcTransactionsByUserResponse{}
	err = u.nc.Request(ctx, SelectProcTransactionsByUser, req, resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	txs = resp.Data
	return
}

const SelectProcBalance = "handler.SelectProcBalance"

func (u *uC) SelectProcBalance(ctx lg.CtxLogger, req *protobuf.SelectProcBalanceRequest) (balance float64, err error) {
	var resp = &protobuf.SelectProcBalanceResponse{}
	err = u.nc.Request(ctx, SelectProcBalance, req, resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	balance = resp.Balance
	return
}

const SelectTurnover = "handler.SelectTurnover"

func (u *uC) SelectTurnover(ctx lg.CtxLogger, req *protobuf.SelectTurnoverRequest) (turnover *protobuf.Turnover, err error) {
	var resp = &protobuf.SelectTurnoverResponse{}
	err = u.nc.Request(ctx, SelectTurnover, req, resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	if resp.Data == nil {
		err = fmt.Errorf("empty address")
		return
	}
	turnover = resp.Data
	return
}

const SelectUserCommission = "handler.SelectUserCommission"

func (u *uC) SelectUserCommission(ctx lg.CtxLogger, req *protobuf.SelectUserCommissionRequest) (userCommission *protobuf.UserCommission, err error) {
	var resp = &protobuf.SelectUserCommissionResponse{}
	err = u.nc.Request(ctx, SelectUserCommission, req, resp)
	if err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	if resp.Data == nil {
		err = fmt.Errorf("empty address")
		return
	}
	userCommission = resp.Data
	return
}

const BatchByCrypto = "handler.BatchByCrypto"

func (u *uC) BatchByCrypto(ctx lg.CtxLogger, req *protobuf.BatchByCryptoRequest) (txs []*protobuf.Crypto, err error) {
	resp := &protobuf.BatchByCryptoResponse{}
	if err = u.nc.Request(ctx, BatchByCrypto, req, resp); err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	if resp.Data == nil {
		err = fmt.Errorf("empty data")
		return
	}

	txs = resp.Data

	return txs, nil
}

const SelectBalanceHistory = "handler.SelectBalanceHistory"

func (u *uC) SelectBalanceHistory(ctx lg.CtxLogger, req *protobuf.SelectBalanceHistoryReq) (balances []*protobuf.BalanceHistory, err error) {
	resp := &protobuf.SelectBalanceHistoryResponse{}
	if err = u.nc.Request(ctx, SelectBalanceHistory, req, resp); err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	balances = resp.Data
	return
}

const SelectMargin = "handler.SelectMargin"

func (u *uC) SelectMargin(ctx lg.CtxLogger, req *protobuf.SelectMarginReq) (balance float64, err error) {
	resp := &protobuf.SelectMarginResponse{}
	if err = u.nc.Request(ctx, SelectMargin, req, resp); err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	balance = resp.Amount
	return
}

const CreateTransferBalance = "handler.CreateTransferBalance"

func (u *uC) CreateTransferBalance(ctx lg.CtxLogger, req *protobuf.TransferBalanceCreateRequest) (res string, err error) {
	resp := &protobuf.TransferBalanceCreateResponse{}
	if err = u.nc.Request(ctx, CreateTransferBalance, req, resp); err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	res = resp.Reason
	return
}

const TransferBalanceUpdate = "handler.TransferBalanceUpdate"

func (u *uC) UpdateTransferBalance(ctx lg.CtxLogger, req *protobuf.TransferBalanceUpdateRequest) (res string, err error) {
	resp := &protobuf.TransferBalanceUpdateResponse{}
	if err = u.nc.Request(ctx, TransferBalanceUpdate, req, resp); err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	res = resp.Reason
	return
}

const SelectTransferBalance = "handler.SelectTransferBalances"

func (u *uC) TransferBalances(ctx lg.CtxLogger, req *protobuf.TransferBalancesRequest) (res *protobuf.TransferLogData, err error) {
	resp := &protobuf.TransferBalancesResponse{}
	if err = u.nc.Request(ctx, SelectTransferBalance, req, resp); err != nil {
		return
	}
	if !resp.Success {
		err = fmt.Errorf(resp.Reason)
		return
	}
	res = resp.Data
	return
}

const SwapInfo = "handler.SwapInfo"

func (u *uC) SwapInfo(ctx lg.CtxLogger, req *protobuf.SwapInfoReq) (resp *protobuf.SwapInfoData, err error) {
	output := &protobuf.SwapInfoResp{}
	if err = u.nc.Request(ctx, SwapInfo, req, output); err != nil {
		return
	}
	if !output.Success {
		err = fmt.Errorf(output.Reason)
		return
	}
	if output.GetData() == nil {
		err = fmt.Errorf("nil data")
		return
	}
	resp = output.GetData()
	return
}

const SelectSwapByUser = "handler.SelectSwapByUser"

func (u *uC) SelectSwapByUser(ctx lg.CtxLogger, req *protobuf.SelectSwapByUserReq) (txs *protobuf.ProcSwapData, err error) {
	output := &protobuf.SelectSwapByUserResp{}
	if err = u.nc.Request(ctx, SelectSwapByUser, req, output); err != nil {
		return
	}
	if !output.Success {
		err = fmt.Errorf(output.Reason)
		return
	}
	if output.GetData() == nil {
		err = fmt.Errorf("nil data")
		return
	}
	txs = output.GetData()
	return
}

const BatchBySwap = "handler.BatchBySwap"

func (u *uC) BatchBySwap(ctx lg.CtxLogger, req *protobuf.BatchBySwapReq) (txs []*protobuf.ProcSwap, err error) {
	output := &protobuf.BatchBySwapResp{}
	if err = u.nc.Request(ctx, BatchBySwap, req, output); err != nil {
		return
	}
	if !output.Success {
		err = fmt.Errorf(output.Reason)
		return
	}
	if output.GetData() == nil {
		err = fmt.Errorf("nil data")
		return
	}
	txs = output.GetData()
	return
}

const SelectProcTransactionsByUsers = "handler.SelectProcTransactionsByUsers"

func (u *uC) SelectProcTransactionsByUsers(ctx lg.CtxLogger, req *protobuf.SelectProcTransactionsByUsersReq) (resp []*protobuf.ProcTransaction, err error) {
	output := &protobuf.SelectProcTransactionsByUsersResp{}
	if err = u.nc.Request(ctx, SelectProcTransactionsByUsers, req, output); err != nil {
		return
	}
	if !output.Success {
		err = fmt.Errorf(output.Reason)
		return
	}
	if output.GetData() == nil {
		err = fmt.Errorf("nil data")
		return
	}
	resp = output.GetData()
	return
}

const GetProcTransactionByTrackerID = "handler.GetProcTransactionByTrackerID"

func (u *uC) GetProcTransactionByTrackerID(ctx lg.CtxLogger, req *protobuf.GetProcTransactionByTrackerIDReq) (resp *protobuf.ProcTransaction, err error) {
	output := &protobuf.GetProcTransactionByTrackerIDResp{}
	if err = u.nc.Request(ctx, GetProcTransactionByTrackerID, req, output); err != nil {
		return
	}
	if !output.Success {
		err = fmt.Errorf(output.Reason)
		return
	}
	if output.GetData() == nil {
		err = fmt.Errorf("nil data")
		return
	}
	resp = output.GetData()
	return
}

const SelectCommissions = "handler.SelectCommissions"

func (u *uC) SelectCommissions(ctx lg.CtxLogger, req *protobuf.SelectCommissionsReq) (resp []*protobuf.Commission, err error) {
	output := &protobuf.SelectCommissionsResp{}
	if err = u.nc.Request(ctx, SelectCommissions, req, output); err != nil {
		return
	}
	if !output.Success {
		err = fmt.Errorf(output.Reason)
		return
	}
	if output.GetData() == nil {
		err = fmt.Errorf("nil data")
		return
	}
	resp = output.GetData()
	return
}
